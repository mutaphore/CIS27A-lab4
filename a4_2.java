/*
@author: Dewei Chen 
@date: 2-2-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a4_2.java
@description: This program converts user input of pounds
to ounces. It will continue to run until user enters a
negative number for the number of pounds.
*/

import java.util.Scanner;

public class a4_2 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		//Conversion factor from pounds to ounces
		final double conversionFactor = 16;
		double pounds;
		
		//Loop to end if user enters a negative #
		do{
		System.out.print("Enter pounds (negative # to quit): ");
		pounds = input.nextDouble();
		if(pounds>=0)
			System.out.printf("%.2f ounces\n", pounds * conversionFactor);
		}while(pounds>=0);

	}

}
