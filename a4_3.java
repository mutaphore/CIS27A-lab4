/*
@author: Dewei Chen 
@date: 2-2-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a4_3.java
@description: This program keeps track of how many people 
of what gender are attending a theater belongs to each of 
5 age categories. The user will enter a number of ages, 
and if the person is M (male) or F (female), entering a 
negative number when there are no more ages to enter. It 
will then report on how many people in each age group attended,
as well as number of males and females. The program will 
also find the average age of the people in attendance, age 
of the oldest and youngest person in attendance.
*/

import java.util.Scanner;

public class a4_3 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		int age = 0;
		int agesum = 0;
		int youngest = 0;
		int oldest = 0;
		int count0to18 = 0;
		int count19to30 = 0;
		int count31to40 = 0;
		int count41to60 = 0;
		int count60plus = 0;
		int countMale = 0;
		int countFemale = 0;
		int averageAge = 0;
		String gender;
		
		for (int i=0 ; age >= 0 ; i++){
			
			System.out.print("Enter age of attendee (negative number to quit): ");
			age = input.nextInt();
			//if a negative age is entered, immediately exit loop before doing more calculations
			if (age < 0)
				break;
			System.out.print("Enter gender (M or F): ");
			gender = input.next();
			//Add to male or female counters when M or F is entered
			if (gender.equals("M") || gender.equals("m"))
				countMale++;
			else 
				countFemale++;
			
			//Only in the first loop, set youngest and oldest age to the age entered 
			if (i == 0)
				youngest = oldest = age;
			//All other loops compare youngest and oldest to age inputted and change their value accordingly
			else{ 
				if (youngest > age)
					youngest = age;
				if (oldest < age)
					oldest = age;
			}
			
			//Classify age inputted to the correct age group
			if (0 <= age && age <= 18)
				count0to18++;
			else if (19 <= age && age <= 30)
				count19to30++;
			else if (31 <= age && age <= 40)
				count31to40++;
			else if (41 <= age && age <= 60)
				count41to60++;
			else
				count60plus++;
			
			//Add up all the ages to compute average age later
			agesum += age;
			
		}
		
		//Calculate average age only when agesum > 0 to avoid dividing by a 0 attendee count
		if (agesum != 0)
			averageAge = agesum / (count0to18 + count19to30 + count31to40 + count41to60 + count60plus);
		
		System.out.println("\nage 0 to 18: " + count0to18);
		System.out.println("age 19 to 30: " + count19to30);
		System.out.println("age 31 to 40: " + count31to40);
		System.out.println("age 41 to 60: " + count41to60);
		System.out.println("over 60: " + count60plus);
		System.out.println("\nmales: " + countMale);
		System.out.println("females: " + countFemale);
		System.out.printf("The average age was %d.\n", averageAge);
		System.out.printf("The youngest person in attendance was %d.\n", youngest);
		System.out.printf("The oldest person in attendance was %d.", oldest);
		
	}

}
