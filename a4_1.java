/*
@author: Dewei Chen 
@date: 2-2-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a4_1.java
@description: This program converts user input of pounds
to ounces. It will prompt if the user wants continue running
and stops when the user enters N.
*/

import java.util.Scanner;

public class a4_1 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		//Conversion factor from pounds to ounces
		final double conversionFactor = 16;
		double pounds;
		String loop;
		
		//Loop to end if user enters N when asked to continue
		do{
		System.out.print("Enter pounds: ");
		pounds = input.nextDouble();
		System.out.printf("%.2f ounces\n", pounds * conversionFactor);
		System.out.print("Continue?(Y/N): ");
		loop = input.next();
		}while(!loop.equals("N") && !loop.equals("n"));
		
	}

}
